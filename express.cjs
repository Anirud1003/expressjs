const express = require("express")

const expressApp = express()
const port = process.env.PORT

expressApp.get('/',(request,response) => {
    response.send("Hello Galaxy")
})

expressApp.listen(port,() => {
    console.log(`working http://localhost:${port}`)
})